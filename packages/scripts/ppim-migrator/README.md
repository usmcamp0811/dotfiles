# README

A quick packaging of [ppim-migrator](https://github.com/v411e/ppim-migrator) so I can move from PhotoPrism to Immich


```sh
nix run .#ppim-migrator -- migrate-all-albums
```
