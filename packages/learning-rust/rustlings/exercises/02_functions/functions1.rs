// TODO: Add some function with the name `call_me` without arguments or a return value.
fn call_me() -> String {
    let name = "Al";
    return name.to_string();
}
fn main() {
    call_me(); // Don't change this line
}
