pub mod cli;
pub mod task;
pub mod task_manager;
pub mod tui;

pub fn run() {
    cli::run();
}
