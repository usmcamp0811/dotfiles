Hello Pytest
==================================

This module is here just to be used in the demonstration of how we can write our own tests using Pytest_.

.. _Pytest: https://docs.pytest.org/en/stable/

.. automodule:: random_python_project.hello_world
    :members:
