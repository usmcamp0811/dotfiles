Satisfies: SRG-OS-000057-GPOS-00027, SRG-OS-000205-GPOS-00083</VulnDiscussion><FalsePositives></FalsePositives><FalseNegatives></FalseNegatives><Documentable>false</Documentable><Mitigations></Mitigations><SeverityOverrideGuidance></SeverityOverrideGuidance><PotentialImpacts></PotentialImpacts><ThirdPartyTools></ThirdPartyTools><MitigationControl></MitigationControl><Responsibility></Responsibility><IAControls></IAControls>DPMS Target Anduril NixOSDISADPMS TargetAnduril NixOS5658CCI-000162CCI-001312Configure the syslog-ng service to create log files with a mode of "0640" or less permissive.

Note: By specifying these configurations in the "options" sections, these configurations apply globally and take effect when these configuration options are not specified later on specific file-based destinations. If there are also related configurations specified at the destination level, these will either need to be updated or removed such that the global specifications apply.

Add or update the "services.syslog-ng.extraConfig" configuration in /etc/nixos/configuration.nix to include the following:

 services.syslog-ng.extraConfig = ''
   options {
    perm(0640);
   };
  '';

Rebuild the NixOS configuration with the following command:

$ sudo nixos-rebuild switchVerify that syslog log files have a mode of "0640" or less permissive by executing the following command:

$ cat $(egrep -o 'cfgfile=.*\.conf' $(nix-store --query --requisites /run/current-system | grep syslog-ng.service)/syslog-ng.service | awk -F '=' '{print $2}') | grep -w perm

 perm(0640);

If any occurrences of "perm" are not configured as "0640" or less permissive, they are not specified, or they are commented out, this is a finding.SRG-OS-000058-GPOS-00028<GroupDescription></GroupDescription>ANIX-00-000610NixOS audit system must protect login UIDs from unauthorized change.<VulnDiscussion>If audit information were to become compromised, then forensic analysis and discovery of the true source of potentially malicious system activity is impossible to achieve.

Audit information includes all information (e.g., audit records, audit settings, audit reports) needed to successfully audit system activity.

In immutable mode, unauthorized users cannot execute changes to the audit system to potentially hide malicious activity and then put the audit rules back. A system reboot would be noticeable and a system administrator could then investigate the unauthorized changes.

