Satisfies: SRG-OS-000033-GPOS-00014, SRG-OS-000250-GPOS-00093, SRG-OS-000394-GPOS-00174</VulnDiscussion><FalsePositives></FalsePositives><FalseNegatives></FalseNegatives><Documentable>false</Documentable><Mitigations></Mitigations><SeverityOverrideGuidance></SeverityOverrideGuidance><PotentialImpacts></PotentialImpacts><ThirdPartyTools></ThirdPartyTools><MitigationControl></MitigationControl><Responsibility></Responsibility><IAControls></IAControls>DPMS Target Anduril NixOSDISADPMS TargetAnduril NixOS5658CCI-000068CCI-001453CCI-003123Configure NixOS to use only ciphers employing FIPS 140-3 approved algorithms.

To configure OpenSSH, add the following Nix code to the NixOS Configuration usually located in /etc/nixos/configuration.nix:

 services.openssh.setting.Ciphers = [
  "aes256-ctr"
  "aes192-ctr"
  "aes128-ctr"   
 ];

Rebuild the NixOS configuration with the following command:

$ sudo nixos-rebuild switch

A reboot is required for the changes to take effect.Verify NixOS is configured to only use ciphers employing FIPS 140-3 approved algorithms with the following command:

$ grep -R -A 4 openssh.setting.Ciphers
 /etc/nixos/configuration.nix:services.openssh.setting.Ciphers = [
/etc/nixos/configuration.nix-  "aes256-ctr"
/etc/nixos/configuration.nix-  "aes192-ctr"
/etc/nixos/configuration.nix-  "aes128-ctr"
/etc/nixos/configuration.nix- ];
   
If the cipher entries in the "configuration.nix" file have any ciphers other than "aes256-ctr,aes192-ctr,aes128-ctr", the order differs from the example above, they are missing, or commented out, this is a finding.SRG-OS-000037-GPOS-00015<GroupDescription></GroupDescription>ANIX-00-000160The NixOS audit package must be installed.<VulnDiscussion>Without establishing what type of events occurred, it would be difficult to establish, correlate, and investigate the events leading up to an outage or attack.

Audit record content that may be necessary to satisfy this requirement includes, for example, time stamps, source and destination addresses, user/process identifiers, event descriptions, success/fail indications, filenames involved, and access control or flow control rules invoked.

Associating event types with detected events in the operating system audit logs provides a means of investigating an attack; recognizing resource utilization or capacity thresholds; or identifying an improperly configured operating system.

