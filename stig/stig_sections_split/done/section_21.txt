Satisfies: SRG-OS-000057-GPOS-00027, SRG-OS-000206-GPOS-00084</VulnDiscussion><FalsePositives></FalsePositives><FalseNegatives></FalseNegatives><Documentable>false</Documentable><Mitigations></Mitigations><SeverityOverrideGuidance></SeverityOverrideGuidance><PotentialImpacts></PotentialImpacts><ThirdPartyTools></ThirdPartyTools><MitigationControl></MitigationControl><Responsibility></Responsibility><IAControls></IAControls>DPMS Target Anduril NixOSDISADPMS TargetAnduril NixOS5658CCI-000162CCI-001314Configure the syslog-ng service to create directories and logs to be protected from unauthorized read access, by setting the correct owner as "root".

Note: By specifying these configurations in the "options" sections, these configurations apply globally and take effect when these configuration options are not specified later on specific file-based destinations. If there are also related configurations specified at the destination level, these will either need to be updated or removed such that the global specifications apply.

Add or update the "services.syslog-ng.extraConfig" configuration in /etc/nixos/configuration.nix to include the following:

 services.syslog-ng.extraConfig = ''
   options {
    owner(root);
    dir_owner(root);
   };
  '';

Rebuild the NixOS configuration with the following command:

$ sudo nixos-rebuild switchVerify the syslog directories and logs are owned by "root" by executing the following command:

$ cat $(egrep -o 'cfgfile=.*\.conf' $(nix-store --query --requisites /run/current-system | grep syslog-ng.service)/syslog-ng.service | awk -F '=' '{print $2}') | grep owner

 owner(root);
 dir_owner(root);

If any occurrences of "owner" and "dir_owner" are not configured as "root", they are not specified, or they are commented out, this is a finding.SRG-OS-000057-GPOS-00027<GroupDescription></GroupDescription>ANIX-00-000580NixOS syslog directory and logs must be group-owned by root to prevent unauthorized read access.<VulnDiscussion>Only authorized personnel should be aware of errors and the details of the errors. Error messages are an indicator of an organization's operational state or can identify the NixOS system or platform. Additionally, Personally Identifiable Information (PII) and operational information must not be revealed through error messages to unauthorized personnel or their designated representatives.

The structure and content of error messages must be carefully considered by the organization and development team. The extent to which the information system is able to identify and handle error conditions is guided by organizational policy and operational requirements.

