Satisfies: SRG-OS-000051-GPOS-00024, SRG-OS-000342-GPOS-00133, SRG-OS-000479-GPOS-00224</VulnDiscussion><FalsePositives></FalsePositives><FalseNegatives></FalseNegatives><Documentable>false</Documentable><Mitigations></Mitigations><SeverityOverrideGuidance></SeverityOverrideGuidance><PotentialImpacts></PotentialImpacts><ThirdPartyTools></ThirdPartyTools><MitigationControl></MitigationControl><Responsibility></Responsibility><IAControls></IAControls>DPMS Target Anduril NixOSDISADPMS TargetAnduril NixOS5658CCI-000154CCI-001851Configure the operating system to authenticate the remote logging server for off-loading audit logs.

Add or update the "services.syslog-ng.extraConfig" configuration in /etc/nixos/configuration.nix to include the following:

  destination d_network {
   syslog(
    "<remote-logging-server>" port(<port>)
    transport(tls)
    tls(
     cert-file("/var/syslog-ng/certs.d/certificate.crt")
     key-file("/var/syslog-ng/certs.d/certificate.key")
     ca-file("/var/syslog-ng/certs.d/cert-bundle.crt")
     peer-verify(yes)
    )
   );
  };

  log { source(s_local); destination(d_local); destination(d_network); };

For example, an updated configuration of 'services.rsyslogd.extraConfig' would look like the following in /etc/nixos/configuration.nix ('...' denoting that the 'services.rsyslogd.extraConfig' configuration may have other options configured):

 services.rsyslogd.extraConfig = ''
   ...
   destination d_network {
    syslog(
     "<remote-logging-server>" port(<port>)
     transport(tls)
     tls(
      cert-file("/var/syslog-ng/certs.d/certificate.crt")
      key-file("/var/syslog-ng/certs.d/certificate.key")
      ca-file("/var/syslog-ng/certs.d/cert-bundle.crt")
      peer-verify(yes)
     )
    );
   };

   log { source(s_local); destination(d_local); destination(d_network); };
   ...
  '';

Rebuild the NixOS configuration with the following command:

$ sudo nixos-rebuild switchVerify the operating system authenticates the remote logging server for off-loading audit logs.

List the configured destinations with the following command:

$ cat $(egrep -o 'cfgfile=.*\.conf' $(nix-store --query --requisites /run/current-system | grep syslog-ng.service)/syslog-ng.service | awk -F '=' '{print $2}')

@version: 4.4
@include "scl.conf"

options {
 keep-hostname(yes);
 create_dirs(yes);
 owner(root);
 group(root);
 perm(0644);
 dir_owner(root);
 dir_group(root);
 dir_perm(0755);
};

source s_local { system(); internal(); };

destination d_local { file("/var/log/messages"); };
destination d_network {
 syslog(
  "<remote-logging-server>" port(<port>)
  transport(tls)
  tls(
   cert-file("/var/syslog-ng/certs.d/certificate.crt")
   key-file("/var/syslog-ng/certs.d/certificate.key")
   ca-file("/var/syslog-ng/certs.d/cert-bundle.crt")
   peer-verify(yes)
  )
 );
};

log { source(s_local); destination(d_local); destination(d_network); };

If the remote destination does not specify "transport(tls)", the remote destination tls configuration does not specify "peer-verify(yes)" or "peer-verify(required-trusted)", or the lines are commented out, ask the system administrator (SA) to indicate how the audit logs are off-loaded to a different system or media. 

If there is no evidence that the transfer of the audit logs being off-loaded to another system or media is encrypted, this is a finding.SRG-OS-000057-GPOS-00027<GroupDescription></GroupDescription>ANIX-00-000520NixOS audit daemon must generate logs that are group-owned by root.<VulnDiscussion>Only authorized personnel should be aware of errors and the details of the errors. Error messages are an indicator of an organization's operational state or can identify the NixOS system or platform. Additionally, Personally Identifiable Information (PII) and operational information must not be revealed through error messages to unauthorized personnel or their designated representatives.

The structure and content of error messages must be carefully considered by the organization and development team. The extent to which the information system is able to identify and handle error conditions is guided by organizational policy and operational requirements.

