Satisfies: SRG-OS-000004-GPOS-00004, SRG-OS-000254-GPOS-00095, SRG-OS-000344-GPOS-00135, SRG-OS-000348-GPOS-00136, SRG-OS-000349-GPOS-00137, SRG-OS-000350-GPOS-00138, SRG-OS-000351-GPOS-00139, SRG-OS-000352-GPOS-00140, SRG-OS-000353-GPOS-00141, SRG-OS-000354-GPOS-00142, SRG-OS-000122-GPOS-00063, SRG-OS-000358-GPOS-00145</VulnDiscussion><FalsePositives></FalsePositives><FalseNegatives></FalseNegatives><Documentable>false</Documentable><Mitigations></Mitigations><SeverityOverrideGuidance></SeverityOverrideGuidance><PotentialImpacts></PotentialImpacts><ThirdPartyTools></ThirdPartyTools><MitigationControl></MitigationControl><Responsibility></Responsibility><IAControls></IAControls>DPMS Target Anduril NixOSDISADPMS TargetAnduril NixOS5658CCI-000018CCI-001464CCI-001858CCI-001875CCI-001877CCI-001878CCI-001879CCI-001880CCI-001881CCI-001882CCI-001876CCI-001889Configure the system to enable the audit service by adding or updating the following configurations in /etc/nixos/configuration.nix:

 security.auditd.enable = true;
 security.audit.enable = true;

Rebuild the system with the following command:

$ sudo nixos-rebuild switchVerify NixOS has the audit service configured with the following command:

$ grep security.audit /etc/nixos/configuration.nix

 security.auditd.enable = true;
 security.audit.enable = true;

If auditd, and audit are not set to true or lock, this is a finding.SRG-OS-000021-GPOS-00005<GroupDescription></GroupDescription>ANIX-00-000040NixOS must enforce the limit of three consecutive invalid login attempts by a user during a 15-minute time period.<VulnDiscussion>By limiting the number of failed login attempts, the risk of unauthorized system access via user password guessing, otherwise known as brute-force attacks, is reduced. Limits are imposed by locking the account.

