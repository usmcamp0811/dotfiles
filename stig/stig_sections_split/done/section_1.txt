Satisfies: SRG-OS-000298-GPOS-00116, SRG-OS-000096-GPOS-00050, SRG-OS-000297-GPOS-00115, SRG-OS-000480-GPOS-00232</VulnDiscussion><FalsePositives></FalsePositives><FalseNegatives></FalseNegatives><Documentable>false</Documentable><Mitigations></Mitigations><SeverityOverrideGuidance></SeverityOverrideGuidance><PotentialImpacts></PotentialImpacts><ThirdPartyTools></ThirdPartyTools><MitigationControl></MitigationControl><Responsibility></Responsibility><IAControls></IAControls>DPMS Target Anduril NixOSDISADPMS TargetAnduril NixOS5658CCI-002322CCI-000382CCI-002314CCI-000366Configure /etc/nixos/configuration.nix to enforce firewall rules by adding the following configuration settings:

 networking.firewall.enable = true;

Rebuild the system with the following command:

$ sudo nixos-rebuild switchVerify NixOS has the network firewall enabled with the following command:

$ grep firewall.enable /etc/nixos/configuration.nix

 networking.firewall.enable = true;

If "networking.firewall.enable" is not set to "true", is commented out, or is missing, this is a finding.SRG-OS-000002-GPOS-00002<GroupDescription></GroupDescription>ANIX-00-000020NixOS emergency or temporary user accounts must be provisioned with an expiration time of 72 hours or less.<VulnDiscussion>If emergency or temporary user accounts remain active when no longer needed or for an excessive period, these accounts may be used to gain unauthorized access. To mitigate this risk, automated termination of all emergency or temporary accounts must be set upon account creation.

Temporary accounts are established as part of normal account activation procedures when there is a need for short-term accounts without the demand for immediacy in account activation.

Emergency accounts are privileged accounts that are established in response to crisis situations where the need for rapid account activation is required. Therefore, emergency account activation may bypass normal account authorization processes. If these accounts are automatically disabled, system maintenance during emergencies may not be possible, thus adversely affecting system availability. 

Emergency accounts are different from infrequently used accounts (i.e., local login accounts used by the organization's system administrators when network or normal login/access is not available). Infrequently used accounts are not subject to automatic termination dates. Emergency accounts are accounts created in response to crisis situations, usually for use by maintenance personnel. The automatic expiration or disabling time period may be extended as needed until the crisis is resolved; however, it must not be extended indefinitely. A permanent account should be established for privileged users who need long-term maintenance accounts.

