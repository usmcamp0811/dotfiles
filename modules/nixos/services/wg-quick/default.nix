{ lib, config, ... }:
with lib;
with lib.campground;
let
  cfg = config.campground.services.wg-quick;
  removeNameField = options: removeAttrs options [ "name" ];
  importedPeers = import ./peers.nix { };

  # Function to extract peers for a specific interface and merge them
  combinePeers = interfaces: importedPeers:
    mapAttrs (interfaceName: interfaceConfig:
      let
        importedPeersForInterface = importedPeers.${interfaceName}.peers or [ ];
        cleanedPeers =
          # Remove the peer with the same publicKey as cfg.publicKey
          filter (peer: peer.publicKey != cfg.publicKey)
          (map (peer: removeAttrs peer [ "name" ]) importedPeersForInterface);
      in interfaceConfig // {
        peers = (interfaceConfig.peers or [ ]) ++ cleanedPeers;
      }) interfaces;

  # Combine the inline interface peers with the imported peers
  combinedInterfaces = combinePeers cfg.interfaces importedPeers;
  peerOpts = {
    options = {
      publicKey = mkOption {
        example = "xTIBA5rboUvnH4htodjb6e697QjLERt1NAB4mZqp8Dg=";
        type = types.str;
        description = "The base64 public key to the peer.";
      };

      presharedKey = mkOption {
        default = null;
        example = "rVXs/Ni9tu3oDBLS4hOyAUAa1qTWVA3loR8eL20os3I=";
        type = with types; nullOr str;
        description = ''
          Base64 preshared key generated by {command}`wg genpsk`.
          Optional, and may be omitted. This option adds an additional layer of
          symmetric-key cryptography to be mixed into the already existing
          public-key cryptography, for post-quantum resistance.

          Warning: Consider using presharedKeyFile instead if you do not
          want to store the key in the world-readable Nix store.
        '';
      };

      presharedKeyFile = mkOption {
        default = null;
        example = "/private/wireguard_psk";
        type = with types; nullOr str;
        description = ''
          File pointing to preshared key as generated by {command}`wg genpsk`.
          Optional, and may be omitted. This option adds an additional layer of
          symmetric-key cryptography to be mixed into the already existing
          public-key cryptography, for post-quantum resistance.
        '';
      };

      allowedIPs = mkOption {
        example = [ "10.192.122.3/32" "10.192.124.1/24" ];
        type = with types; listOf str;
        description = ''
          List of IP (v4 or v6) addresses with CIDR masks from
                  which this peer is allowed to send incoming traffic and to which
                  outgoing traffic for this peer is directed. The catch-all 0.0.0.0/0 may
                  be specified for matching all IPv4 addresses, and ::/0 may be specified
                  for matching all IPv6 addresses.'';
      };

      endpoint = mkOption {
        default = null;
        example = "demo.wireguard.io:12913";
        type = with types; nullOr str;
        description = ''
          Endpoint IP or hostname of the peer, followed by a colon,
                  and then a port number of the peer.'';
      };

      persistentKeepalive = mkOption {
        default = null;
        type = with types; nullOr int;
        example = 25;
        description = ''
          This is optional and is by default off, because most
                  users will not need it. It represents, in seconds, between 1 and 65535
                  inclusive, how often to send an authenticated empty packet to the peer,
                  for the purpose of keeping a stateful firewall or NAT mapping valid
                  persistently. For example, if the interface very rarely sends traffic,
                  but it might at anytime receive traffic from a peer, and it is behind
                  NAT, the interface might benefit from having a persistent keepalive
                  interval of 25 seconds; however, most users will not need this.'';
      };
    };
  };
  interfaceOpts = { ... }: {
    options = {

      configFile = mkOption {
        example = "/secret/wg0.conf";
        default = null;
        type = with types; nullOr str;
        description = ''
          wg-quick .conf file, describing the interface.
          Using this option can be a useful means of configuring WireGuard if
          one has an existing .conf file.
          This overrides any other configuration interface configuration options.
          See wg-quick manpage for more details.
        '';
      };

      address = mkOption {
        example = [ "192.168.2.1/24" ];
        default = [ ];
        type = with types; listOf str;
        description = "The IP addresses of the interface.";
      };

      autostart = mkOption {
        description =
          "Whether to bring up this interface automatically during boot.";
        default = true;
        example = false;
        type = types.bool;
      };

      dns = mkOption {
        example = [ "192.168.2.2" ];
        default = [ ];
        type = with types; listOf str;
        description = "The IP addresses of DNS servers to configure.";
      };

      privateKey = mkOption {
        example = "yAnz5TF+lXXJte14tji3zlMNq+hd2rYUIgJBgB3fBmk=";
        type = with types; nullOr str;
        default = null;
        description = ''
          Base64 private key generated by {command}`wg genkey`.

          Warning: Consider using privateKeyFile instead if you do not
          want to store the key in the world-readable Nix store.
        '';
      };

      generatePrivateKeyFile = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Automatically generate a private key with
          {command}`wg genkey`, at the privateKeyFile location.
        '';
      };

      privateKeyFile = mkOption {
        example = "/private/wireguard_key";
        type = with types; nullOr str;
        default = null;
        description = ''
          Private key file as generated by {command}`wg genkey`.
        '';
      };

      listenPort = mkOption {
        default = null;
        type = with types; nullOr int;
        example = 51820;
        description = ''
          16-bit port for listening. Optional; if not specified,
          automatically generated based on interface name.
        '';
      };

      preUp = mkOption {
        example =
          literalExpression ''"''${pkgs.iproute2}/bin/ip netns add foo"'';
        default = "";
        type = with types; coercedTo (listOf str) (concatStringsSep "\n") lines;
        description = ''
          Commands called at the start of the interface setup.
        '';
      };

      preDown = mkOption {
        example =
          literalExpression ''"''${pkgs.iproute2}/bin/ip netns del foo"'';
        default = "";
        type = with types; coercedTo (listOf str) (concatStringsSep "\n") lines;
        description = ''
          Command called before the interface is taken down.
        '';
      };

      postUp = mkOption {
        example =
          literalExpression ''"''${pkgs.iproute2}/bin/ip netns add foo"'';
        default = "";
        type = with types; coercedTo (listOf str) (concatStringsSep "\n") lines;
        description = ''
          Commands called after the interface setup.
        '';
      };

      postDown = mkOption {
        example =
          literalExpression ''"''${pkgs.iproute2}/bin/ip netns del foo"'';
        default = "";
        type = with types; coercedTo (listOf str) (concatStringsSep "\n") lines;
        description = ''
          Command called after the interface is taken down.
        '';
      };

      table = mkOption {
        example = "main";
        default = null;
        type = with types; nullOr str;
        description = ''
          The kernel routing table to add this interface's
          associated routes to. Setting this is useful for e.g. policy routing
          ("ip rule") or virtual routing and forwarding ("ip vrf"). Both
          numeric table IDs and table names (/etc/rt_tables) can be used.
          Defaults to "main".
        '';
      };

      mtu = mkOption {
        example = 1248;
        default = null;
        type = with types; nullOr int;
        description = ''
          If not specified, the MTU is automatically determined
          from the endpoint addresses or the system default route, which is usually
          a sane choice. However, to manually specify an MTU to override this
          automatic discovery, this value may be specified explicitly.
        '';
      };

      peers = mkOption {
        default = [ ];
        description = "Peers linked to the interface.";
        type = with types; listOf (submodule peerOpts);
      };
    };
  };
in {
  options.campground.services.wg-quick = with types; {
    enable = mkBoolOpt false "Enable an Tang;";
    publicKey = mkOpt str "public key" "Your public key for easy reference";
    interfaces = mkOption {
      description = ''
        WireGuard interfaces.

        Please note that {option}`systemd.network.netdevs` has more features
        and is better maintained. When building new things, it is advised to
        use that instead.
      '';
      default = {
        campnet = {
          ips = [ "10.8.0.10/24" ];
          privateKeyFile = "/var/lib/wireguard/campnet/private-key";
          peers = [ ];
        };
      };
      example = {
        wg0 = {
          ips = [ "192.168.20.4/24" ];
          privateKeyFile = "/var/lib/wireguard/wg0/private-key";
          peers = [{
            allowedIPs = [ "192.168.20.1/32" ];
            publicKey = "xTIBA5rboUvnH4htodjb6e697QjLERt1NAB4mZqp8Dg=";
            endpoint = "demo.wireguard.io:12913";
          }];
        };
      };
      type = with types; attrsOf (submodule interfaceOpts);
    };
  };

  config =
    mkIf cfg.enable { networking.wg-quick.interfaces = combinedInterfaces; };
}
