# Sergeant Reckless

![Sergeant Reckless](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Sgt_reckless_in_pasture.jpg/440px-Sgt_reckless_in_pasture.jpg)

## Personal Information

**Breed:** Mongolian  
**Discipline:** Pack horse  
**Sex:** Female  
**Foaled:** c. 1948 – South Korea  
**Died:** May 13, 1968 (aged ~20)  
**Buried:** Camp Pendleton, California, U.S.  
**Owner:**

- **Kim Huk Moon** (Original)
- **United States Marine Corps** (Military service)

## Military Service

**Allegiance:** United States  
**Branch:** United States Marine Corps  
**Years of Service:** 1952–1960  
**Rank:** **Staff Sergeant**

### Battles & Wars

- **Battle for Outpost Vegas** (Korean War)

### Awards & Decorations

- **Dickin Medal** _(The animal equivalent of the Victoria Cross)_
- **Purple Heart** (2×)
- **Navy Presidential Unit Citation** (2×)
- **Navy Unit Commendation**
- **Marine Corps Good Conduct Medal**
- **National Defense Service Medal**
- **Korean Service Medal** (4×)
- **United Nations Korea Medal**
- **Animals in War & Peace Medal of Bravery**

## Legacy

Sergeant Reckless was a **warhorse like no other**, serving with the **United States Marine Corps** during
the **Korean War**. Originally purchased for **$250** from a Korean stableboy who needed money to buy
an artificial leg for his sister, Reckless quickly became an **indispensable part of the 5th Marine Regiment's
Recoilless Rifle Platoon**.

During the **Battle for Outpost Vegas**, she made **51 solo trips in a single day**, carrying over **
9,000 pounds of ammunition** across **35 miles** of treacherous battlefield. She also evacuated wounded
Marines and was **wounded twice in combat**. She was later promoted to **Sergeant in 1954** and **Staff
Sergeant in 1959** by the **Commandant of the Marine Corps**.

After the war, Reckless retired to **Camp Pendleton, California**, where she gave birth to **four foals**.
She died in **1968** and is honored with multiple **statues and memorials** across the U.S., including
at the **National Museum of the Marine Corps**.

## Video Tribute

[![Sergeant Reckless - Video Tribute](https://img.youtube.com/vi/NJhxBuLI-eE/0.jpg)](https://www.youtube.com/watch?v=NJhxBuLI-eE)
