# Dan Daly

![Dan Daly](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/DanielDaly.jpg/440px-DanielDaly.jpg)

## Personal Information

**Birth Name:** Daniel Joseph Daly  
**Nickname(s):** _"Fightin' Dan"_  
**Born:** November 11, 1873 – Glen Cove, New York, U.S.  
**Died:** April 27, 1937 (aged 63) – Glen Cove, New York, U.S.  
**Allegiance:** United States  
**Service Branch:** United States Marine Corps  
**Years of Service:** 1899–1929  
**Rank:** Sergeant Major

## Battles & Wars

- **Boxer Rebellion**
  - _Battle of Peking_
- **Banana Wars**
  - _First Occupation of Haiti_
  - _Battle of Fort Dipitie_
  - _Occupation of the Dominican Republic_
- **World War I**
  - _Battle of Belleau Wood_
  - _Battle of Château-Thierry_

## Awards & Decorations

- **Medal of Honor** (2×) _(One of only two Marines to receive it twice for separate actions)_
- **Navy Cross**
- **Distinguished Service Cross**
- **French Médaille militaire**
- **French Croix de Guerre (with Palm)**

## Legacy

Dan Daly is remembered as one of the toughest and most fearless Marines in U.S. history. He was one of
only two Marines ever to receive the **Medal of Honor twice for separate actions**—once during the **
Boxer Rebellion** and again during the **Occupation of Haiti**. His legendary battle cry at **Belleau
Wood**, _"Come on, you sons of bitches, do you want to live forever?"_, became one of the most famous
quotes in Marine Corps history.

Despite his small stature, Daly was known for his ferocity in combat and unwavering courage. His legacy
continues to inspire Marines, and he is widely considered one of the greatest warriors the Corps has
ever produced.

## Video Tribute

[![Dan Daly - Video Tribute](https://img.youtube.com/vi/neuqN6MgsyI/0.jpg)](https://www.youtube.com/watch?v=neuqN6MgsyI)
