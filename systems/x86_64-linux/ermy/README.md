# R. Lee Ermey

![R. Lee Ermey](https://upload.wikimedia.org/wikipedia/commons/4/4f/RLeeErmeyCrop.jpeg)

## Personal Information

**Birth Name:** Ronald Lee Ermey  
**Nickname(s):** _"The Gunny"_  
**Born:** March 24, 1944 – Emporia, Kansas, U.S.  
**Died:** April 15, 2018 (aged 74) – Santa Monica, California, U.S.  
**Allegiance:** United States  
**Service Branch:** United States Marine Corps  
**Years of Service:** 1961–1972  
**Rank:** Staff Sergeant (honorary Gunnery Sergeant)

## Battles & Wars

- **Vietnam War**

## Awards & Decorations

- **Meritorious Unit Commendation**
- **Marine Corps Good Conduct Medal** (3×)
- **National Defense Service Medal**
- **Armed Forces Expeditionary Medal**
- **Vietnam Service Medal** (with Bronze Star)
- **Vietnam Gallantry Cross (with Palm & Frame)**
- **Vietnam Campaign Medal**

## Post-Military Career

After being medically discharged from the Marine Corps, Ermey transitioned into acting, where he became
widely known for his role as **Gunnery Sergeant Hartman** in _Full Metal Jacket_ (1987). His performance
was largely improvised and earned him a **Golden Globe nomination**. He went on to appear in dozens of
films, television shows, and video games, often playing military or authority figures.

Beyond acting, Ermey was a strong advocate for veterans, the Marine Corps, and the Second Amendment.
He hosted several television programs, including _Mail Call_ and _Lock N’ Load with R. Lee Ermey_.

## Legacy

R. Lee Ermey remains an iconic figure in both military and pop culture. His portrayal of **Gunnery Sergeant
Hartman** defined the archetype of the tough, no-nonsense drill instructor, influencing generations of
actors and Marines alike. His continued support for the **U.S. military and veterans** made him a respected
figure beyond Hollywood. In 2002, he was **promoted to Honorary Gunnery Sergeant** by the **Commandant
of the Marine Corps**, cementing his lasting legacy in the Corps.
