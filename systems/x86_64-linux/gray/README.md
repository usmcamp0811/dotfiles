# Al Gray

![Al Gray](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/Official_Portrait_of_Retired_Gen._Alfred_M._Gray_%282%29_%28cropped%29.jpg/440px-Official_Portrait_of_Retired_Gen._Alfred_M._Gray_%282%29_%28cropped%29.jpg)

## Personal Information

**Birth Name:** Alfred Mason Gray Jr.  
**Born:** June 22, 1928 – Rahway, New Jersey, U.S.  
**Died:** March 20, 2024 (aged 95) – Alexandria, Virginia, U.S.  
**Buried:** Arlington National Cemetery  
**Allegiance:** United States  
**Service Branch:** United States Marine Corps  
**Years of Service:** 1950–1991  
**Rank:** General

## Commands Held

- **Commandant of the Marine Corps**
- **Marine Forces Atlantic**
- **II Marine Expeditionary Force**
- **2nd Marine Division**
- **33rd Marine Amphibious Unit**
- **4th Marine Regiment**
- **2nd Marine Regiment**
- **1st Battalion, 2nd Marines**
- **1st Radio Battalion**

## Battles & Wars

- **Korean War**
- **Vietnam War**

## Awards & Decorations

- **Defense Distinguished Service Medal** (2×)
- **Navy Distinguished Service Medal** (2×)
- **Army Distinguished Service Medal**
- **Air Force Distinguished Service Medal**
- **Coast Guard Distinguished Service Medal**
- **Silver Star**
- **Legion of Merit** (2×)
- **Bronze Star Medal** (4×)
- **Purple Heart** (3×)

## Legacy

General **Al Gray** served as the **29th Commandant of the Marine Corps (1987–1991)** and played a critical
role in **modernizing Marine Corps doctrine and structure**. He championed **maneuver warfare**, emphasizing
**agility, adaptability, and intelligence** in combat operations. His leadership reshaped Marine Corps
strategy, ensuring its preparedness for modern conflicts.

Gray's emphasis on **warfighting philosophy and decentralization** continues to shape **Marine Corps
training and leadership**. Even after retirement, he remained an influential figure in **military thought
and Marine Corps traditions**. His legacy as a visionary leader and warfighter is honored at **Arlington
National Cemetery**.
