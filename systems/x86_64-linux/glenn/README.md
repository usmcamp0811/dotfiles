# John Glenn

![John Glenn](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/John_Glenn_Low_Res.jpg/440px-John_Glenn_Low_Res.jpg)

## Personal Information

**Birth Name:** John Herschel Glenn Jr.  
**Born:** July 18, 1921 – Cambridge, Ohio, U.S.  
**Died:** December 8, 2016 (aged 95) – Columbus, Ohio, U.S.  
**Buried:** Arlington National Cemetery  
**Allegiance:** United States  
**Service Branch:** United States Marine Corps  
**Years of Service:** 1942–1965  
**Rank:** Colonel

## Commands Held

- **VMF-155 (Fighter Squadron)**
- **VMF-311 (Marine Fighter Squadron)**
- **Naval Test Pilot School**

## Battles & Wars

- **World War II**
- **Korean War**

## Awards & Decorations

- **Congressional Space Medal of Honor**
- **Presidential Medal of Freedom**
- **Distinguished Flying Cross** (6×)
- **Air Medal** (18×)
- **Navy Unit Commendation**
- **Asiatic-Pacific Campaign Medal**
- **Korean Service Medal**

## Legacy

Colonel **John Glenn** was a **Marine Corps aviator, test pilot, astronaut, and U.S. senator**. He made
history as the **first American to orbit the Earth** aboard **Friendship 7 in 1962**, a mission that
solidified America's place in the space race.

After retiring from NASA and the Marine Corps, Glenn served as a **U.S. Senator from Ohio (1974–1999)**,
advocating for **science, education, and space exploration**. In 1998, at age **77**, he returned to
space aboard **Space Shuttle Discovery**, becoming the **oldest person to fly in space**.

Glenn's pioneering spirit, service to his country, and contributions to both **military aviation and
space exploration** cement his place as an **American hero**, honored at **Arlington National Cemetery**.
