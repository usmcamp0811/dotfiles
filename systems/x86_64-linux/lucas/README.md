# Jacklyn Harold Lucas

![Jack Lucas](https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Lucas_JH.jpg/440px-Lucas_JH.jpg)

## Personal Information

**Birth Name:** Jacklyn Harold Lucas  
**Nickname(s):** _Jack_  
**Born:** February 14, 1928 – Plymouth, North Carolina, U.S.  
**Died:** June 5, 2008 (aged 80) – Hattiesburg, Mississippi, U.S.  
**Buried:** Highland Cemetery, Hattiesburg, Mississippi  
**Allegiance:** United States of America  
**Service Branch:**

- **United States Marine Corps** (1942–1945)
- **United States Army** (1961–1965)  
  **Rank:**
- **Private First Class** (U.S. Marine Corps)
- **Captain** (U.S. Army)

## Units Served

- **1st Battalion, 26th Marine Regiment, 5th Marine Division**
- **82nd Airborne Division**

## Battles & Wars

- **World War II**
  - _Battle of Iwo Jima_

## Awards & Decorations

- **Medal of Honor**
- **Purple Heart Medal**

## Legacy

Jack Lucas is one of the most legendary figures in Marine Corps history. At **just 17 years old**, he
became the **youngest Medal of Honor recipient of World War II**, earning the award for **heroic actions
on Iwo Jima**. After sneaking into the military at just **14 years old**, he was deployed to the Pacific
and threw himself on two grenades to save his fellow Marines. Miraculously, he survived.

After the war, Lucas later served in the **U.S. Army's 82nd Airborne Division**, where he survived a
**parachute failure during training**. His incredible bravery and resilience made him an **icon of selfless
heroism** in both the **Marine Corps and American military history**.

## Video Tribute

[![Jack Lucas - Video Tribute](https://img.youtube.com/vi/bW0rsKayU8k/0.jpg)](https://www.youtube.com/watch?v=bW0rsKayU8k)
