# Chesty Puller

![Chesty Puller](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Chesty-puller.jpg/440px-Chesty-puller.jpg)

## Personal Information

**Birth Name:** Lewis Burwell Puller  
**Nickname(s):** _Chesty_  
**Born:** June 26, 1898 – West Point, Virginia, U.S.  
**Died:** October 11, 1971 (aged 73) – Hampton, Virginia, U.S.  
**Allegiance:** United States  
**Service Branch:** United States Marine Corps  
**Years of Service:** 1918–1955  
**Rank:** Lieutenant General

## Commands Held

- 1st Battalion, 7th Marines
- 1st Marine Regiment
- 3rd Marine Division
- Marine Barracks, Pearl Harbor

## Battles & Wars

- **Banana Wars**
  - _Occupation of Haiti_
  - _Occupation of Nicaragua_
- **World War II**
  - _Battle of Guadalcanal_
  - _Battle of Cape Gloucester_
  - _Battle of Peleliu_
- **Korean War**
  - _Battle of Pusan Perimeter_
  - _Battle of Inchon_
  - _Battle of Chosin Reservoir_

## Awards & Decorations

- **Navy Cross** (5×) _(Most in Marine Corps history)_
- **Distinguished Service Cross**
- **Silver Star**
- **Legion of Merit** (with Combat "V")
- **Bronze Star Medal** (with Combat "V")
- **Purple Heart**

## Legacy

Chesty Puller is the most decorated Marine in U.S. history, known for his fearless leadership and aggressive
combat tactics. A legend among Marines, he led troops through some of the most brutal battles of the
**20th century**, from the **Banana Wars** to **World War II** and the **Korean War**. His leadership
at the _Battle of Chosin Reservoir_ became a defining moment in Marine Corps history.

Puller’s larger-than-life persona and his unwavering commitment to his men made him a beloved figure
in the Marine Corps. To this day, recruits at **Marine Corps boot camp** end their day with the chant:

_"Good night, Chesty, wherever you are!"_
