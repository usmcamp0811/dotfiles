# Smedley Butler

![Smedley Butler](https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/SmedleyButler.jpeg/440px-SmedleyButler.jpeg)

## Personal Information

**Birth Name:** Smedley Darlington Butler  
**Nickname(s):** _Maverick Marine_, _Old Gimlet Eye_, _The Fighting Quaker_, _Fighting Hell-Devil_  
**Born:** July 30, 1881 – West Chester, Pennsylvania, U.S.  
**Died:** June 21, 1940 (aged 58) – Philadelphia, Pennsylvania, U.S.  
**Allegiance:** United States  
**Service Branch:** United States Marine Corps  
**Years of Service:** 1898–1931  
**Rank:** Major General

## Commands Held

- 3rd Battalion, 1st Marines
- 13th Marines
- Marine Barracks, Quantico
- Marine Corps Base, San Diego
- 3rd Marine Brigade
- Marine Barracks, Quantico

## Battles & Wars

- **Spanish–American War**
- **Philippine–American War**
- **Boxer Rebellion**
  - _Battle of Tientsin (WIA)_
- **Banana Wars**
  - _Occupation of Nicaragua_
  - _Occupation of Veracruz_
  - _Occupation of Haiti_
- **World War I**
  - _Defensive Sector_

## Awards & Decorations

- **Medal of Honor** (2×)
- **Marine Corps Brevet Medal**
- **Military Medal (Haiti)**
- **Commander of the Order of the Black Star (France)**

## Legacy

Smedley Butler was one of the most decorated Marines in U.S. history, earning two Medals of Honor for
bravery. After retiring, he became a vocal critic of U.S. military interventionism, exposing how war
primarily served corporate and elite interests in his book _War Is a Racket_. His legacy as both a fearless
warrior and an outspoken advocate against military-industrial corruption continues to influence military
and political discourse today.
