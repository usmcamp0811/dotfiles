# Jim Webb

![Jim Webb](https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Jim_Webb_official_110th_Congress_photo.jpg/440px-Jim_Webb_official_110th_Congress_photo.jpg)

## Personal Information

**Full Name:** James Henry Webb Jr.  
**Born:** February 9, 1946 (age 79) – St. Joseph, Missouri, U.S.  
**Political Party:** Democratic (since 2006)  
**Other Affiliations:** Republican (before 2006)  
**Spouses:**

- Barbara Samorajczyk _(m. 1968; div. 1979)_
- Jo Ann Krukar _(m. 1981; div. 2004)_
- Hong Le Webb _(m. 2005–present)_  
  **Children:** 6 (1 stepchild)  
  **Education:**
- United States Naval Academy _(BS)_
- Georgetown University _(JD)_

---

## Military Service

**Allegiance:** United States  
**Branch:** United States Marine Corps  
**Years of Service:** 1968–1972  
**Rank:** **Captain**  
**Unit:** **Delta Company, 1st Battalion, 5th Marines**

### Battles & Wars

- **Vietnam War** _(Wounded in Action - WIA)_

### Awards & Decorations

- **Navy Cross**
- **Silver Star**
- **Bronze Star (2×)**
- **Purple Heart (2×)**

---

## Political Career

### **United States Senator from Virginia**

- **Term:** January 3, 2007 – January 3, 2013
- **Preceded by:** George Allen
- **Succeeded by:** Tim Kaine

### **66th United States Secretary of the Navy**

- **Term:** May 1, 1987 – February 23, 1988
- **President:** Ronald Reagan
- **Preceded by:** John Lehman
- **Succeeded by:** William Ball

### **1st Assistant Secretary of Defense for Reserve Affairs**

- **Term:** May 3, 1984 – April 10, 1987
- **President:** Ronald Reagan
- **Preceded by:** Position established
- **Succeeded by:** Stephen Duncan

---

## Other Roles & Accomplishments

- **Former Counsel, U.S. House Committee on Veterans' Affairs**
- **Journalist, Filmmaker, & Author of 10 Books**
- **Taught Literature at the U.S. Naval Academy**
- **Fellow at Harvard Institute of Politics**
- **First Distinguished Fellow, University of Notre Dame International Security Center (2020)**
